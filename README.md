# ics-ans-role-tenable-lce

Ansible role to install tenable-lce.

## Role Variables

```yaml
log_correlation_engine_url: http://artifactory.esss.lu.se/artifactory/swi-pkg/Tenable/lce-6.0.2-el7.x86_64.rpm
log_correlation_engine_daemon: lce_www.service
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-tenable-lce
```

## License

BSD 2-clause
